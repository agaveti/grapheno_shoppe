$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "grapheno_shoppe/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "grapheno_shoppe"
  s.version     = GraphenoShoppe::VERSION
  s.authors     = ["Diana Enriquez"]
  s.email       = ["diana@graphenosolutions.com "]
  s.homepage    = "TODO"
  s.summary     = "Summary of GraphenoShoppe."
  s.description = "Description of GraphenoShoppe."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 4.1.4"

  s.add_dependency "carrierwave"
  s.add_dependency "rmagick"

  s.add_development_dependency "pg"

end

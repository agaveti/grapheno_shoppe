module GraphenoShoppe
  class Engine < ::Rails::Engine
    isolate_namespace GraphenoShoppe
  end

  require 'carrierwave'

end
